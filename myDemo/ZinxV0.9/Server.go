package main

import (
	"fmt"
	"mmogame/ziface"
	"mmogame/znet"
)

/*
基于Zinx框架来开发的服务器端应用程序
*/
type PingRouter struct {
	znet.BaseRouter
}

//Ping Handle
func (this *PingRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call PingRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping...
	fmt.Println("recv from client: msgId=", request.GetMsgID(), ",data=", string(request.GetData()))

	//回写数据
	if err := request.GetConnection().SendBuffMsg(1, []byte("ping...ping...ping")); err != nil {
		fmt.Println(err)
	}
}

type HelloZinxRouter struct {
	znet.BaseRouter
}

func (this *HelloZinxRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call HelloZinx Handle")
	//先读取客户端的数据，再回写ping...ping...ping...
	fmt.Println("recv from client: msgId=", request.GetMsgID(), ",data=", string(request.GetData()))

	//回写数据
	if err := request.GetConnection().SendBuffMsg(201, []byte("Hello Zinx Router V0.9")); err != nil {
		fmt.Println(err)
	}
}

/*

创建OnConnStart钩子函数
*/
func DoConnectionBegin(conn ziface.IConnection) {
	fmt.Println("=============================>DoConnectionBegin is Called ...")
	if err := conn.SendBuffMsg(202, []byte("DoConnection BEGIN")); err != nil {
		fmt.Println(err)
	}
}

/*

创建OnConnStart钩子函数
*/
func DoConnectionStop(conn ziface.IConnection) {
	fmt.Println("=============================>DoConnectionStop is Called...")
	fmt.Println("conn ID=", conn.GetConnId(), "is Lost...")
}

func main() {
	//1.创建一个server句柄,使用Zinx的api
	s := znet.NewServer("[Zinx V0.9]")

	//2.注册连接Hook钩子函数
	s.SetOnConnStart(DoConnectionBegin)
	s.SetOnConnStop(DoConnectionStop)

	//3.给当前zinx框架新增一个自定义的router
	s.AddRouter(0, &PingRouter{})
	s.AddRouter(1, &HelloZinxRouter{})

	//4.启动server
	s.Serve()

}
