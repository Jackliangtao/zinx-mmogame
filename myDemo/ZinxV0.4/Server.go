package main

import (
	"fmt"
	"mmogame/ziface"
	"mmogame/znet"
)

/*
基于Zinx框架来开发的服务器端应用程序
*/
type PingRouter struct {
	znet.BaseRouter
}

//Test PreHandle
func (this *PingRouter) PreHandle(request ziface.IRequest) {
	fmt.Println("Call Router PreHandle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("before ping ....  "))
	if err != nil {
		fmt.Println("call back ping ping ping error")
	}
}

//Test Handle
func (this *PingRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call PingRouter Handle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("ping...ping...ping "))
	if err != nil {
		fmt.Println("call back ping ping ping error")
	}
}

//Test PostHandle
func (this *PingRouter) PostHandle(request ziface.IRequest) {
	fmt.Println("Call Router PostHandle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("After ping ..... "))
	if err != nil {
		fmt.Println("call back ping ping ping error")
	}
}

func main() {
	//1.创建一个server句柄,使用Zinx的api
	s := znet.NewServer("[zinx V0.4]")

	//2.给当前zinx框架新增一个自定义的router
	s.AddRouter(0, &PingRouter{})

	//3.启动server
	s.Serve()

}
