package main

import (
	"fmt"
	"mmogame/ziface"
	"mmogame/znet"
)

/*
基于Zinx框架来开发的服务器端应用程序
*/
type PingRouter struct {
	znet.BaseRouter
}

//Ping Handle
func (this *PingRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call PingRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping...
	fmt.Println("recv from client: msgId=", request.GetMsgID(), ",data=", string(request.GetData()))

	//回写数据
	if err := request.GetConnection().SendMsg(1, []byte("ping...ping...ping")); err != nil {
		fmt.Println(err)
	}
}

type HelloZinxRouter struct {
	znet.BaseRouter
}

func (this *HelloZinxRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call HelloZinx Handle")
	//先读取客户端的数据，再回写ping...ping...ping...
	fmt.Println("recv from client: msgId=", request.GetMsgID(), ",data=", string(request.GetData()))

	//回写数据
	if err := request.GetConnection().SendMsg(201, []byte("Hello Zinx Router V0.7")); err != nil {
		fmt.Println(err)
	}
}

func main() {
	//1.创建一个server句柄,使用Zinx的api
	s := znet.NewServer("[Zinx V0.7]")

	//2.给当前zinx框架新增一个自定义的router
	s.AddRouter(0, &PingRouter{})
	s.AddRouter(1, &HelloZinxRouter{})

	//3.启动server
	s.Serve()

}
