package main

import (
	"fmt"
	"mmogame/ziface"
	"mmogame/znet"
)

/*
基于Zinx框架来开发的服务器端应用程序
*/
type PingRouter struct {
	znet.BaseRouter
}

//Ping Handle
func (this *PingRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call PingRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping...
	fmt.Println("recv from client: msgId=", request.GetMsgID(), ",data=", string(request.GetData()))

	//回写数据
	if err := request.GetConnection().SendBuffMsg(1, []byte("ping...ping...ping")); err != nil {
		fmt.Println(err)
	}
}

type HelloZinxRouter struct {
	znet.BaseRouter
}

func (this *HelloZinxRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call HelloZinx Handle")
	//先读取客户端的数据，再回写ping...ping...ping...
	fmt.Println("recv from client: msgId=", request.GetMsgID(), ",data=", string(request.GetData()))

	//回写数据
	if err := request.GetConnection().SendBuffMsg(201, []byte("Hello Zinx Router V1.0")); err != nil {
		fmt.Println(err)
	}
}

/*

创建OnConnStart钩子函数
*/
func DoConnectionBegin(conn ziface.IConnection) {
	fmt.Println("=============================>DoConnectionBegin is Called ...")
	//给当前连接设置一些属性
	//=============设置两个链接属性，在连接创建之后===========
	fmt.Println("Set conn Name, Home done!")
	conn.SetProperty("Name", "Jackliang")
	conn.SetProperty("Home", "https://gitee.com/Jackliangtao/zinx-mmogame")
	//===================================================

	if err := conn.SendBuffMsg(202, []byte("DoConnection BEGIN")); err != nil {
		fmt.Println(err)
	}
}

/*

创建OnConnStart钩子函数
*/
func DoConnectionStop(conn ziface.IConnection) {
	//============在连接销毁之前，查询conn的Name，Home属性=====
	if name, err := conn.GetProperty("Name"); err == nil {
		fmt.Println("Conn Property Name = ", name)
	}

	if home, err := conn.GetProperty("Home"); err == nil {
		fmt.Println("Conn Property Home = ", home)
	}
	//===================================================
	fmt.Println("=============================>DoConnectionStop is Called...")
	fmt.Println("conn ID=", conn.GetConnId(), "is Lost...")
}

func main() {
	//1.创建一个server句柄,使用Zinx的api
	s := znet.NewServer("[Zinx V1.0]")

	//2.注册连接Hook钩子函数
	s.SetOnConnStart(DoConnectionBegin)
	s.SetOnConnStop(DoConnectionStop)

	//3.给当前zinx框架新增一个自定义的router
	s.AddRouter(0, &PingRouter{})
	s.AddRouter(1, &HelloZinxRouter{})

	//4.启动server
	s.Serve()

}
