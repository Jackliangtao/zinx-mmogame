package main

import "mmogame/znet"

/*
基于Zinx框架来开发的服务器端应用程序
*/

func main() {
	//1.创建一个server句柄,使用Zinx的api
	s := znet.NewServer("[zinx V0.1]")

	//TODO 做一些启动服务器之后的额外业务

	//2.启动server
	s.Serve()

}
