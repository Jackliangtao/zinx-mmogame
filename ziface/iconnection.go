package ziface

import "net"

//定义链接木块的抽象层

type IConnection interface {
	//启动连接
	Start()

	//停止连接
	Stop()

	//获取当前连接绑定的socket conn
	GetTCPConnection() *net.TCPConn

	//获取当前连接模块的连接ID
	GetConnId() uint32

	//获取远程客户端的TCP状态 IP Port
	RemoteAddr() net.Addr

	//直接将发送数据到远程客户端（无缓冲）
	SendMsg(msgId uint32, data []byte) error

	//直接将Message数据发送到远程的TCP客户端（有缓冲）
	SendBuffMsg(msgId uint32, data []byte) error

	//设置连接属性
	SetProperty(key string, value interface{})

	//获取连接属性
	GetProperty(key string) (interface{}, error)

	//移除连接属性
	RemoveProperty(key string)
}

//定义一个处理链接业务的方法
type HandleFunc func(*net.TCPConn, []byte, int) error
