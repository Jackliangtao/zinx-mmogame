package ziface

//定义一个服务器接口
type IServer interface {
	//启动服务器
	Start()
	//停止服务器
	Stop()
	//运行服务器
	Serve()
	//给当前的服务器注册一个路由方法供客户端连接处理使用
	AddRouter(msgId uint32, router IRouter)

	//得到连接管理
	GetConnMgr() IConnManager

	//设置创建连接时的Hook函数
	SetOnConnStart(func(IConnection))

	//设置销毁连接时的Hook函数
	SetOnConnStop(func(IConnection))

	//调用连接OnConnStart Hook函数
	CallOnConnStart(conn IConnection)

	//调用连接OnConnStop Hook函数
	CallOnConnStop(conn IConnection)
}
