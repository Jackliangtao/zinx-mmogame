package znet

import (
	"errors"
	"fmt"
	"mmogame/ziface"
	"sync"
)

/*
	连接管理模块
*/

type ConnManager struct {
	connections map[uint32]ziface.IConnection //管理的链接信息
	connLock    sync.RWMutex                  //读写连接的读写锁
}

/*
	创建一个链接管理
*/
func NewConnManager() *ConnManager {
	return &ConnManager{
		connections: make(map[uint32]ziface.IConnection),
	}
}

/*
添加连接
*/
func (connMgr *ConnManager) Add(conn ziface.IConnection) {
	//并发写map前加锁
	connMgr.connLock.Lock()
	defer connMgr.connLock.Unlock()

	//将conn连接加入到connManager中
	connMgr.connections[conn.GetConnId()] = conn
	fmt.Println("connection add to ConnManager successfully :conn num=", connMgr.Len())
}

//删除连接
func (connMgr *ConnManager) Remove(conn ziface.IConnection) {
	//保护共享资源Map 加写锁
	connMgr.connLock.Lock()
	defer connMgr.connLock.Unlock()

	//删除连接信息
	delete(connMgr.connections, conn.GetConnId())

	fmt.Println("connection Remove ConnID=", conn.GetConnId(), " successfully: conn num = ", connMgr.Len())
}

//利用connID得到连接
func (connMgr *ConnManager) Get(connID uint32) (ziface.IConnection, error) {
	//并发写map前加读锁
	connMgr.connLock.RLock()
	defer connMgr.connLock.RUnlock()

	//如果map中存在connID对应连接
	if conn, ok := connMgr.connections[connID]; ok {
		return conn, nil
	} else {
		return nil, errors.New("connection not found")
	}

}

//获取当前连接总数
func (connMgr *ConnManager) Len() int {
	return len(connMgr.connections)
}

//清除并停止所有连接
func (connMgr *ConnManager) ClearConn() {
	//并发写map前加锁
	connMgr.connLock.Lock()
	defer connMgr.connLock.Unlock()

	//遍历map中的所有连接，Stop并删除
	for connId, conn := range connMgr.connections {
		conn.Stop()
		delete(connMgr.connections, connId)
	}

	fmt.Println("Clear All Connection successfully :conn num =", connMgr.Len())
}
