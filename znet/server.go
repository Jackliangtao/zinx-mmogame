package znet

import (
	"fmt"
	"mmogame/utils"
	"mmogame/ziface"
	"net"
	"time"
)

//IServer的接口实现，定义一个Server的服务器模块
type Server struct {
	//服务器的名称
	Name string

	//服务器绑定的ip版本
	IPVersion string

	//服务器监听的IP
	IP string

	//服务器监听的端口
	Port int

	//当前Server由用户绑定的回调router,也就是Server注册的链接对应的处理业务
	MsgHandle ziface.IMsgHandle

	//当前Server的连接管理模块
	ConnMgr ziface.IConnManager

	/*
		新增两个hook函数原型
	*/
	OnConnStart func(conn ziface.IConnection)
	OnConnStop  func(conn ziface.IConnection)
}

func (s *Server) AddRouter(msgId uint32, router ziface.IRouter) {
	s.MsgHandle.AddRouter(msgId, router)
	fmt.Println("Add Router Succ!!")
}

//启动服务器
func (s *Server) Start() {
	fmt.Printf("[START] Server name: %s,listenner at IP: %s, Port %d is starting\n", s.Name, s.IP, s.Port)
	fmt.Printf("[Zinx] Version: %s, MaxConn: %d,  MaxPacketSize: %d\n",
		utils.GlobalObject.Version,
		utils.GlobalObject.MaxConn,
		utils.GlobalObject.MaxPacketSize)
	fmt.Printf("[Start]Server Listenner at IP: %s, Port: %d, is starting\n", s.IP, s.Port)
	go func() {

		//0. 启动worker工作池机制
		s.MsgHandle.StartWorkerPool()
		//1、获取一个TCP的Addr
		addr, err := net.ResolveTCPAddr(s.IPVersion, fmt.Sprintf("%s:%d", s.IP, s.Port))
		if err != nil {
			fmt.Println("resolve tcp addr error", err)
			return
		}
		//2、监听服务器的地址
		listener, err := net.ListenTCP(s.IPVersion, addr)
		if err != nil {
			fmt.Println("listen ", s.IPVersion, " err ", err)
			return
		}
		fmt.Println("start Zinx server succ,", s.Name, "succ,Listenning...")
		var cid uint32
		cid = 0
		//3、阻塞的等待客户端连接，处理客户端连接业务
		for {
			conn, err := listener.AcceptTCP()
			if err != nil {
				fmt.Println("Accepet err", err)
				continue
			}

			//设置最大连接个数的判断，如果超过最大连接数量，就关闭此连接
			if s.ConnMgr.Len() >= utils.GlobalObject.MaxConn {
				//给客户端响应一个超出最大连接的错误包
				fmt.Println("Too Many Connections ,MaxConn=", utils.GlobalObject.MaxConn)
				conn.Close()
				continue
			}

			//已经与客户端建立连接，添加业务
			dealConn := NewConnection(s, conn, cid, s.MsgHandle)
			cid++

			//启动当前的连接业务处理
			go dealConn.Start()
		}
	}()
}

func (s *Server) Stop() {
	fmt.Println("[Stop]Zinx server,name=", s.Name)
	//将其他需要清理的连接信息或者其他信息 也要一并停止或者清理
	s.ConnMgr.ClearConn()
}

func (s *Server) Serve() {
	s.Start()

	//阻塞状态
	for {
		time.Sleep(10 * time.Second)
	}
}

//得到连接管理
func (s *Server) GetConnMgr() ziface.IConnManager {
	return s.ConnMgr
}

//设置该Server的连接创建时Hook函数
func (s *Server) SetOnConnStart(hookFunc func(ziface.IConnection)) {
	s.OnConnStart = hookFunc
}

//设置该Server的连接销毁时Hook函数
func (s *Server) SetOnConnStop(hookFunc func(ziface.IConnection)) {
	s.OnConnStop = hookFunc
}

//调用OnConnStart Hook 函数
func (s *Server) CallOnConnStart(conn ziface.IConnection) {
	if s.OnConnStart != nil {
		fmt.Println("---->CallOnConnStart....")
		s.OnConnStart(conn)
	}
}

//调用OnConnStart Hook 函数
func (s *Server) CallOnConnStop(conn ziface.IConnection) {
	if s.OnConnStop != nil {
		fmt.Println("---->CallOnConnStop....")
		s.OnConnStop(conn)
	}
}

/*
初始化Server模块的方法
*/
func NewServer(name string) ziface.IServer {
	s := &Server{
		Name:      utils.GlobalObject.Name,
		IPVersion: "tcp4",
		IP:        utils.GlobalObject.Host,
		Port:      utils.GlobalObject.TcpPort,
		MsgHandle: NewMsgHandle(),
		ConnMgr:   NewConnManager(),
	}

	return s
}
