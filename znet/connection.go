package znet

import (
	"errors"
	"fmt"
	"io"
	"mmogame/utils"
	"mmogame/ziface"
	"net"
	"sync"
)

type Connection struct {
	//当前连接的socket TCP套接字
	Conn *net.TCPConn

	//连接的ID
	ConnID uint32

	//当前连接的状态
	isClosed bool

	//告知当前连接已经退出的Channel
	ExitBuffChan chan bool

	//消息的管理MsgId和对应的处理业务API关系
	//Router ziface.IRouter
	MsgHandle ziface.IMsgHandle

	//添加Reader和Writer之间通信的channel(无缓冲）
	msgChan chan []byte

	//Reader和Writer之间通信的channel(有缓冲）
	msgBufferChan chan []byte

	//当前Conn属于哪个Server
	TcpServer ziface.IServer //当前conn属于哪个server，在conn初始化的时候添加即可

	//连接属性的集合
	property map[string]interface{}

	//保护连接属性的互斥锁
	propertyLock sync.RWMutex
}

func (c *Connection) SetProperty(key string, value interface{}) {
	c.propertyLock.Lock()
	defer c.propertyLock.Unlock()

	//添加一个链接属性
	c.property[key] = value
}

func (c *Connection) GetProperty(key string) (interface{}, error) {
	c.propertyLock.RLock()
	defer c.propertyLock.RUnlock()

	//获取连接属性
	if value, ok := c.property[key]; ok {
		return value, nil
	} else {
		return nil, errors.New("Property Not Found!!!")
	}
}

func (c *Connection) RemoveProperty(key string) {
	c.propertyLock.Lock()
	defer c.propertyLock.Unlock()

	//删除属性
	delete(c.property, key)
}

//初始化连接模块的方法
func NewConnection(server ziface.IServer, conn *net.TCPConn, connId uint32, msgHandler ziface.IMsgHandle) *Connection {
	c := &Connection{
		TcpServer:     server,
		Conn:          conn,
		ConnID:        connId,
		isClosed:      false,
		MsgHandle:     msgHandler,
		ExitBuffChan:  make(chan bool, 1),
		msgChan:       make(chan []byte), //msgChan初始化
		msgBufferChan: make(chan []byte, utils.GlobalObject.MaxMsgChanLen),
		property:      make(map[string]interface{}), //连接属性 初始化
	}

	//将新创建的Connection添加到ConnManager中
	c.TcpServer.GetConnMgr().Add(c)
	return c
}

//启动连接
func (c *Connection) Start() {
	fmt.Println("Conn Start()...ConnID==", c.ConnID)
	//开启处理该链接读取到客户端数据之后的请求业务
	go c.StartReader()
	//开启从当前连接写数据的业务
	go c.StartWriter()

	//按照用户传递进来的创建连接时需要处理的业务，执行钩子方法
	c.TcpServer.CallOnConnStart(c)
	for {
		select {
		case <-c.ExitBuffChan:
			//得到退出消息，不再阻塞
			return
		}
	}

}

//链接的读业务方法
func (c *Connection) StartReader() {
	fmt.Println("[Reader Goroutine is  running]")
	defer fmt.Println(c.RemoteAddr().String(), " [conn Reader exit!]")
	defer c.Stop()

	for {
		/*//读取我们最大的数据到buf中
		buf := make([]byte, utils.GlobalObject.MaxPacketSize)
		_, err := c.Conn.Read(buf)
		if err != nil {
			fmt.Println("recv buf err ", err)
			c.ExitBuffChan <- true
			continue
		}*/

		//创建一个拆包解包的对象
		dp := NewDataPack()
		//读取客户端的Msg Head二进制流8个字节
		headData := make([]byte, dp.GetHeadLen())
		if _, err := io.ReadFull(c.GetTCPConnection(), headData); err != nil {
			fmt.Println("read msg head error", err)
			/*c.ExitBuffChan <- true
			continue*/
			break
		}

		//拆包 得到msgId和msgDatalen 放在msg消息中
		msg, err := dp.UnPack(headData)
		if err != nil {
			fmt.Println("unpack error", err)
			/*c.ExitBuffChan <- true
			continue*/
			break
		}

		//根据msgDataLen再次读取Data
		var data []byte
		if msg.GetDataLen() > 0 {
			data = make([]byte, msg.GetDataLen())
			if _, err := io.ReadFull(c.GetTCPConnection(), data); err != nil {
				fmt.Println("read msg data error", err)
				/*c.ExitBuffChan <- true
				continue*/
				break
			}
		}

		msg.SetData(data)

		//得到当前conn数据的request请求数据
		req := Request{
			conn: c,
			msg:  msg,
		}

		//从绑定好的消息和对应的处理方法中执行对应的Handle方法
		if utils.GlobalObject.WorkerPoolSize > 0 {
			//已经启动工作池机制，将消息交给Worker处理
			c.MsgHandle.SendMsgToTaskQueue(&req)
		} else {
			//从绑定好的消息和对应的处理方法中执行对应的Handle方法
			go c.MsgHandle.DoMsgHandler(&req)
		}
	}
}

/*
	写消息Goroutine， 用户将数据发送给客户端
*/
func (c *Connection) StartWriter() {
	fmt.Println("Writer Goroutine is  running")
	defer fmt.Println(c.RemoteAddr().String(), " conn writer exit!")

	//不断地阻塞等待channel的消息
	for {
		select {
		case data := <-c.msgChan:
			//有数据要写给客户端
			if _, err := c.Conn.Write(data); err != nil {
				fmt.Println("Send Data error:", err)
				return
			}

		//针对有缓冲channel需要的数据处理：
		case data, ok := <-c.msgBufferChan:
			if ok {
				//有数据要写给客户端
				if _, err := c.Conn.Write(data); err != nil {
					fmt.Println("Send Buff Data error:, ", err, " Conn Writer exit")
					return
				} else {
					break
					fmt.Println("msgBuffChan is Closed")
				}
			}
		case <-c.ExitBuffChan:
			return

		}

	}

}

//停止连接
func (c *Connection) Stop() {
	fmt.Println("Conn Stop()...ConnID==", c.ConnID)
	//如果当前连接已经关闭
	if c.isClosed == true {
		return
	}
	c.isClosed = true

	//==================
	//如果用户注册了该链接的关闭回调业务，那么在此刻应该显示调用
	c.TcpServer.CallOnConnStop(c)

	//调用关闭socket连接
	c.Conn.Close()

	c.ExitBuffChan <- true
	//将连接从连接管理器中删除
	c.TcpServer.GetConnMgr().Remove(c)

	//关闭管道
	close(c.ExitBuffChan)
	close(c.msgChan)
}

func (c *Connection) GetTCPConnection() *net.TCPConn {
	return c.Conn
}

func (c *Connection) GetConnId() uint32 {
	return c.ConnID
}

//获取远程客户端的TCP状态 IP Port
func (c *Connection) RemoteAddr() net.Addr {
	return c.Conn.RemoteAddr()
}

//封包，然后发送数据到远程客户端
func (c *Connection) SendMsg(msgId uint32, data []byte) error {
	if c.isClosed == true {
		return errors.New("Connection closed when send msg")
	}

	//将data进行封包

	dp := NewDataPack()
	binaryMsg, err := dp.Pack(NewMessage(msgId, data))

	if err != nil {
		fmt.Println("Pack error msg id = ", msgId)
		return errors.New("Pack error msg ")
	}

	//将数据写回客户端
	c.msgChan <- binaryMsg

	return nil
}

func (c *Connection) SendBuffMsg(msgId uint32, data []byte) error {
	if c.isClosed == true {
		return errors.New("Connection closed when send Buff msg")
	}
	//将data封包，并且发送
	dp := NewDataPack()
	msg, err := dp.Pack(NewMessage(msgId, data))
	if err != nil {
		fmt.Println("Pack error msg id = ", msgId)
		return errors.New("Pack error msg ")
	}

	//写回客户端
	c.msgBufferChan <- msg

	return nil
}
